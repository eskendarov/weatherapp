package ru.eskendarov.weatherapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        logger("onStart()");
    }

    private void logger(final Object o) {
        Log.d("TestBug", String.format("%s", o));
    }

    @Override
    protected void onStop() {
        super.onStop();
        logger("onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logger("onDestroy()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        logger("onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        logger("onResume()");
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // обновление меню
    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        // setGroupVisible(ID группы меню, видимость boolean значение)
        menu.setGroupVisible(R.id.topPanel, true);
        return super.onPrepareOptionsMenu(menu);
    }

    // Обработка нажатий
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        Toast.makeText(this, "Selected: " + item.getTitle(),
                Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(item);
    }

}